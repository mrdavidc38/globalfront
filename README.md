# GlobalFront

Este proyecto fue creado con la versión  16 de Angular CLI y la versión 16 de node, tambien se usó la libreria Angular material en su version 16 para diseño de sus componentes, al igual que NGRX para consumo de servicios y gestión estados usando el Store, y por ultimo la arquitetura esta basado en concepto de "Arquitectura limpia". 

El aplicativo cuenta con un Login, y dos modulos de gestión de usuarios y tickets, cada user con sus correspondientes tickets; dentro del repositorio tambien está su Dockerfile para generar la imagen Docker.

Existen dos usuarios en BD para logueo, este es un ejemplo con uno de ellos 
https://gitlab.com/mrdavidc38/globalfront/-/blob/main/globalLogin.png


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
Z
import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
  })
export class BaseApiService {
    private url = environment.apiUrl;
    constructor(private readonly http: HttpClient) { }

    get(path: string, headers?:HttpHeaders) {
      return this.http.get(`${this.url}${path}`);
    }

      post(path?: string, data?: any, headers?:HttpHeaders) {
 
    return this.http.post(`${this.url}${path}`, data, { headers : headers } );
  }

//     encryptUsingAES256(data: string) {
//     var encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(JSON.stringify(data)), this.key, {
//       keySize: 128 / 8,
//       iv: this.iv,
//       mode: CryptoJS.mode.CBC,
//       padding: CryptoJS.pad.Pkcs7
//     });
//    return encrypted;}
}
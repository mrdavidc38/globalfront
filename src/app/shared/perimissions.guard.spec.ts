import { TestBed } from '@angular/core/testing';
import { CanActivateFn } from '@angular/router';

import { perimissionsGuard } from './perimissions.guard';

describe('perimissionsGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => perimissionsGuard(...guardParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from '../modal/modal.component';
import { DialogModalCustomDate } from '../domain/entities/dialog-modal-custom-date';

@Injectable({
  providedIn: 'root'
})
export class ModalServiceService {

  constructor(private modalActual :MatDialog)
{}

openDialogCustom()
{
  this.modalActual.open(ModalComponent);
}
opendialogtemplate(data : DialogModalCustomDate)
{
  return this.modalActual.open(ModalComponent, {data});
}

closeDialogtemplate()
{
  return this.modalActual.closeAll();
}
}

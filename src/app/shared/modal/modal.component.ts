import { Component, Inject, inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { DialogModalCustomDate } from '../domain/entities/dialog-modal-custom-date';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent {
constructor(@Inject(MAT_DIALOG_DATA)  public data: DialogModalCustomDate)
{

}
}

import { ActivatedRouteSnapshot, CanActivate, CanActivateFn, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { LoginOut } from '../domain/methodParameters/LoginOut';
import { getAll } from 'src/app/store/selector/user.selector';
@Injectable({
  providedIn: 'root'
})
export class permissions implements CanActivate  {
  logOut : any;
  constructor(private router:Router,private store: Store<LoginOut>)
  {}
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
      this.store.select(getAll).pipe().subscribe(local =>{
        this.logOut = local;
          })
   if(this.logOut.user.token !='')
   {
    return true;
   }
  else{
    this.router.navigate(["Login"])
    return false;}
  }
  
  
};

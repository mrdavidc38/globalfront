import { User } from "../entities/User";


export interface CreateUserIn {
    user: User;
}
import { Result } from "../entities/Result";

export interface BaseResponse {
    result?: Result;
    message?: string;
  }
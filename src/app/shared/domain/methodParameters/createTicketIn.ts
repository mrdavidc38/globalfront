import { Ticket } from "../entities/Ticket";

export interface createTicketsIn {
    createTicket: Ticket;
}

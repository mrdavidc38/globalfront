import { BaseResponse } from "./BaseResponse";

export interface CreateUserOut extends BaseResponse {
    userId: number;
}
import { User } from "../entities/User";

export interface GetUserIn {
    user: User;
}
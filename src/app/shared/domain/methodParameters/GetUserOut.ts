import { User } from "../entities/User";
import { BaseResponse } from "./BaseResponse";


export interface GetUserOut extends BaseResponse {
    user: User;
}
import { User } from "../entities/User";
import { BaseResponse } from "./BaseResponse";


export interface GetUserByIDOut extends BaseResponse {
    user: User[];
}
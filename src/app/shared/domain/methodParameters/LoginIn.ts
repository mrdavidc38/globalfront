export interface LoginIn{
    documentType: number;
    usuName: string;
    password: string;
    usuEmail: string;
}
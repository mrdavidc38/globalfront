import { BaseResponse } from "./BaseResponse";


export interface LoginOut extends BaseResponse {
    token: string;
    name: string;
    firstSurname: string;
    secondSurname: string;
    usuID: number;
    proID: number;
    usuPhone: string;
    proName: string;
}
export interface DeleteUserIn {
    userId: number;
    userDeleteId: number;
}
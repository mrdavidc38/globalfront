import { Ticket } from "../entities/Ticket";
import { BaseResponse } from "./BaseResponse";


export interface getTicketsOut extends BaseResponse {
    getTicket: Ticket[];
}
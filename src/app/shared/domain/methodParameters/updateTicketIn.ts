import { Ticket } from "../entities/Ticket";

export interface updateTicketIn {
    updateTicket: Ticket;
}
import { User } from "../entities/User";


export interface UpdateUserIn {
    user: User;
}
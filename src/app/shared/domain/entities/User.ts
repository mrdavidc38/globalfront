import { Profile } from "./Profile";

export interface User {
    usuID: number;
    usuName: string;
    usuEmail: string;
    usuStatus: string;
   
    usuUserCreation: number;
    usuPassword: string;
    usuUserMod: number;
    usuUserModDate: number;
    usuCreationDate: Date;
    proID: number;
}
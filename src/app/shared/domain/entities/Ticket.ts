export interface Ticket {
    ticID: number;
    usuID: number;
    tic_description: string;
    tic_usuName: string;
    tic_arrivalDate: Date;
    tic_departureDate: Date;
    tic_status: string;
    tic_creationDate: Date;
    tic_userCreation: string;
    tic_modDate: Date;
    tic_userMod: string;
}
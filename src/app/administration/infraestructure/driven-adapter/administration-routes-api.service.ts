import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, map } from "rxjs";
import { BaseApiService } from "src/app/shared/base-api.service";
import { AdministrationGateway } from "../../domain/gateway/administration-routes-gateway";
import { LoginOut } from "src/app/shared/domain/methodParameters/LoginOut";
import { LoginIn } from "src/app/shared/domain/methodParameters/LoginIn";

@Injectable()
export class AdministrationRoutesApiService extends AdministrationGateway{

  constructor(private http: BaseApiService,
    private https: HttpClient) { super();}


   Login(path? : string, input? : LoginIn ): Observable<LoginOut> {
    return this.http.post(path, input).pipe(map((Response => Response as LoginOut)));
  }

  
}
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../ui/login/login.component';


const routes: Routes = [
  {path: 'Login',
  component : LoginComponent,
  loadChildren :() => import('src/app/administration/administration.module').then(module => module.AdministrationModule)
  
}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrationRoutingModule { }

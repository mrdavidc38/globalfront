import { Component, OnInit } from '@angular/core';
import { AdministrationUsecase } from '../../domain/usecase/administration-use-case';
import { LoginIn } from 'src/app/shared/domain/methodParameters/LoginIn';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as loginDataActionTypes from 'src/app/store/Actions/user.actions';
         
import { getAll } from 'src/app/store/selector/user.selector';
import { environment } from 'src/app/environments/environment';
import { Result } from 'src/app/shared/domain/entities/Result';
import { LoginOut } from 'src/app/shared/domain/methodParameters/LoginOut';
import { Store } from '@ngrx/store';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent  implements OnInit {
  formularioLogin! : FormGroup;
  ocultarPassword : boolean = true;
  monstrarLoadinfg : boolean = false;
  token? : any ;
  // private key = CryptoJS.enc.Utf8.parse(environment.key);
  // private iv = CryptoJS.enc.Utf8.parse(environment.key);
  constructor(private _administationUseCase : AdministrationUsecase,
    private fb : FormBuilder, private router : Router,
    private store: Store<LoginOut>,)
  {
    this.formularioLogin = fb.group({
      email : [, Validators.required],
      password : [, Validators.required],
      name : [, Validators.required]
    }); 
  }


  ngOnInit(): void {
 
  }

  iniciarSesion()
  {
    
    // var pass = this.formularioLogin.get('password')?.value;
    // const passENC = this.encryptUsingAES256(pass);

    var login : LoginIn = {
      documentType: 0,
      usuName: this.formularioLogin.get('name')?.value,
      password: this.formularioLogin.get('password')?.value,
      usuEmail: this.formularioLogin.get('email')?.value
    }
    
    this._administationUseCase.Login('',login).subscribe(
      { next: (data) => {
        if(data.result == Result.Success){
          
          
          this.store.dispatch(loginDataActionTypes.update(data));

         
          
        }
      },complete: ()=> {
        setTimeout(() => {
          
          this.store.select(getAll).pipe().subscribe(local =>
            {
              this.token = local;
             if(this.token.user.token)
             {
              this.router.navigate(["pages/user"])
             }
            else{
              this.router.navigate(["Login"])
            }
            });

        }, 300);


          
      }
    }
    );
  }

}



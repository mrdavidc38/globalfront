import { Observable } from "rxjs";
import { LoginIn } from "src/app/shared/domain/methodParameters/LoginIn";
import { LoginOut } from "src/app/shared/domain/methodParameters/LoginOut";

export abstract class AdministrationGateway{
    abstract Login(path? : string, input? : LoginIn): Observable<LoginOut>;
} 
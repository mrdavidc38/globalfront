import { Injectable } from "@angular/core";
import { AdministrationGateway } from "../gateway/administration-routes-gateway";
import { LoginIn } from "src/app/shared/domain/methodParameters/LoginIn";
@Injectable()
export class AdministrationUsecase {
    constructor(private _administrationGateway : AdministrationGateway)
    {  
    }

    Login(path? : string, input? : LoginIn)
    {
        return this._administrationGateway.Login('authentication/Login', input);
    }
}
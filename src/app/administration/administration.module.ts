import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdministrationRoutingModule } from './infraestructure/administration-routing.module';
import { LoginComponent } from './ui/login/login.component';
import { SharedModule } from '../shared/shared.module';
import { AdministrationUsecase } from './domain/usecase/administration-use-case';
import { AdministrationGateway } from './domain/gateway/administration-routes-gateway';
import { AdministrationRoutesApiService } from './infraestructure/driven-adapter/administration-routes-api.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';



@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    AdministrationRoutingModule,
    SharedModule,
    MatCardModule,
    HttpClientModule
  ]
  ,
  exports : [
    LoginComponent
  ],
  
  providers: [
    AdministrationUsecase,
    {provide: AdministrationGateway, useClass: AdministrationRoutesApiService}]
})
export class AdministrationModule { }
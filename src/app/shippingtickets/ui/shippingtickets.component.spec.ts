import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShippingticketsComponent } from './shippingtickets.component';

describe('ShippingticketsComponent', () => {
  let component: ShippingticketsComponent;
  let fixture: ComponentFixture<ShippingticketsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ShippingticketsComponent]
    });
    fixture = TestBed.createComponent(ShippingticketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

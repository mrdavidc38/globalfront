import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Ticket } from 'src/app/shared/domain/entities/Ticket';
import { LoginOut } from 'src/app/shared/domain/methodParameters/LoginOut';
import { ModalServiceService } from 'src/app/shared/modalService/modal-service.service';
import { ShippingUseCase } from '../domain/usecase/shipping-use-case';
import { getTicektsIn } from 'src/app/shared/domain/methodParameters/getTicektsIn';
import { getAll } from 'src/app/store/selector/user.selector';
import { updateTicketIn } from 'src/app/shared/domain/methodParameters/updateTicketIn';
import { createTicketsIn } from 'src/app/shared/domain/methodParameters/createTicketIn';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { deleteTicketIn } from 'src/app/shared/domain/methodParameters/deleteTicketIn';
import { Result } from 'src/app/shared/domain/entities/Result';

@Component({
  selector: 'app-shippingtickets',
  templateUrl: './shippingtickets.component.html',
  styleUrls: ['./shippingtickets.component.css']
})
export class ShippingticketsComponent implements OnInit  {

   d = new Date('yyyy-MM-dd')
   piker = true;
  dataInicio : Ticket[]=[{ticID:0, usuID:2,tic_description:'description',tic_usuName:'nombre', tic_arrivalDate:this.d, tic_departureDate : this.d, tic_status:'status', tic_creationDate: this.d,tic_userCreation:'user', tic_modDate:this.d,tic_userMod:'' }];
  columnasTabla = ['Numero','Description', 'Usuario', 'Hora de salida', 'Hora de llegada', 'Estado', 'acciones' ]
  tituliAction = '';
  formularioTicketEdit: any;
  formularioTicketCreate: any;
  logOut : any;
  botonActionEdit = 'Editar';
  botonActionCreate = 'Crear';
  ocultarPassword = true;
  dataSource = new MatTableDataSource<Ticket>(this.dataInicio);
  @ViewChild("MatPaginator") paginator!: MatPaginator;
  constructor(private fb : FormBuilder,private _modal : ModalServiceService,
    private _shipping : ShippingUseCase,
     private router : Router, private store: Store<LoginOut>)
  {
    this.formularioTicketEdit = fb.group({
      ticID :[],
      description :[,Validators.required],
      name :[,Validators.required],
      arrival :[,Validators.required],
      departure :[,Validators.required],
      estado :[,Validators.required]
    });

    this.formularioTicketCreate = fb.group({
      ticID :[],
      description :[,Validators.required],
      name :[,Validators.required],
      arrival :[,Validators.required],
      departure :[,Validators.required],
      estado :[,Validators.required]
    });

    this.store.select(getAll).pipe().subscribe(local =>{
      this.logOut = local;
        });
  }
  ngOnInit(): void {

this.getATicketByID();
 
  }
  openModal(template: TemplateRef<any>){
this._modal.opendialogtemplate({template : template}).afterClosed().subscribe({});
  }

  aplicarFiltroTabla(input : Event)
  {}

  eliminarTicket(element : Ticket)
  {
    var deletre : deleteTicketIn =
    {
      usuID: element.ticID
    } 

    this._shipping.DeleteTicket('',deletre, this.logOut.user.token).subscribe({
      next :(data)=>{
          if(data.result == Result.Success)
          {}
      },
      complete: ()=>
      {
        this.getATicketByID();
        this.modalCLose();
      }
    });
  }

  editarTicket(element : Ticket,template: TemplateRef<any>)
  {
    this.formularioTicketEdit.get('ticID').patchValue(element.ticID);
    this.formularioTicketEdit.get('description').patchValue(element.tic_description);
    this.formularioTicketEdit.get('name').patchValue(element.tic_usuName);
    this.formularioTicketEdit.get('arrival').patchValue(element.tic_arrivalDate);
    this.formularioTicketEdit.get('departure').patchValue(element.tic_departureDate);
    this.formularioTicketEdit.get('estado').patchValue(element.tic_status == "Activo" ? "1":"2");

    this._modal.opendialogtemplate({template:template}).afterClosed().subscribe({});
  }
  Editar_Ticket()
  {

    var d = new Date();

    var upodateT : Ticket = {
      ticID: this.formularioTicketEdit.get('ticID').value,
      usuID: 0,
      tic_description: this.formularioTicketEdit.get('description').value,
      tic_usuName: this.formularioTicketEdit.get('name').value,
      tic_arrivalDate: this.formularioTicketEdit.get('arrival').value,
      tic_departureDate: this.formularioTicketEdit.get('departure').value,
      tic_status: this.formularioTicketEdit.get('estado').value == '1' ?'V':'I',
      tic_creationDate: d ,
      tic_userCreation: '',
      tic_modDate: d ,
      tic_userMod: ''
    }
    var update : updateTicketIn ={
      updateTicket: upodateT
    }

    this._shipping.UpdateTicket('',update, this.logOut.user.token).subscribe({
      next: (data) =>{},
      complete:() =>{
        this.getATicketByID();
        this.modalCLose();
      }
    })

  }
  modalCLose()
  {
    this._modal.closeDialogtemplate();
  }

  guardar_Ticket()
  {
    var d = new Date();

    var upodateT : Ticket = {
      ticID: 0,
      usuID: this.logOut.user.usuID,
      tic_description: this.formularioTicketCreate.get('description').value,
      tic_usuName: this.formularioTicketCreate.get('name').value,
      tic_arrivalDate: this.formularioTicketCreate.get('arrival').value,
      tic_departureDate: this.formularioTicketCreate.get('departure').value,
      tic_status: this.formularioTicketCreate.get('estado').value == 1 ? 'Activo': 'Inactivo',
      tic_creationDate: d ,
      tic_userCreation: '',
      tic_modDate: d ,
      tic_userMod: ''
    }
    var create : createTicketsIn ={
      createTicket: upodateT
    }

    this._shipping.CreateTicket('',create, this.logOut.user.token).subscribe({
      next: (data) =>{},
      complete:() =>{
        this.getATicketByID();
        this.modalCLose();
      }
    })
  }
  getAllTicket()
{
  var date = Date()
  var getTicket : Ticket ={
    ticID: 0,
    usuID: this.logOut.user.usuID,
    tic_description: '',
    tic_usuName: '',
    tic_arrivalDate: this.d,
    tic_departureDate: this.d,
    tic_status: '',
    tic_creationDate: this.d,
    tic_userCreation: '',
    tic_modDate: this.d,
    tic_userMod: ''
  }
var getTicekts : getTicektsIn ={
getTicket: getTicket
}



  this._shipping.GetTicketAll('',getTicekts, this.logOut.user.token).subscribe({
    next: (data) => {
      
      this.dataInicio = data.getTicket;
    }
  })
}

getATicketByID()
{
  var date = Date()
  var getTicket : Ticket ={
    ticID: 0,
    usuID: this.logOut.user.usuID,
    tic_description: '',
    tic_usuName: '',
    tic_arrivalDate: this.d,
    tic_departureDate: this.d,
    tic_status: '',
    tic_creationDate: this.d,
    tic_userCreation: '',
    tic_modDate: this.d,
    tic_userMod: ''
  }
var getTicekts : getTicektsIn ={
getTicket: getTicket
}



  this._shipping.GetTicketByID('',getTicekts, this.logOut.user.token).subscribe({
    next: (data) => {
      
      this.dataInicio = data.getTicket;
    }
  })
}
ngAfterViewInit() {
  this.dataSource.paginator = this.paginator;
}
}

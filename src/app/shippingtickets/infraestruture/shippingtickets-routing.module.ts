import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShippingticketsComponent } from '../ui/shippingtickets.component';

const routes: Routes = [
  {
    path: 'tickets',
    component : ShippingticketsComponent,
    loadChildren : () => import('src/app/shippingtickets/shippingtickets.module').then(module => module.ShippingticketsModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShippingticketsRoutingModule { }

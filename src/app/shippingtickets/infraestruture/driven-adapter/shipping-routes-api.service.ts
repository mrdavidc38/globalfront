import { Observable, map } from "rxjs";
import { createTicketsIn } from "src/app/shared/domain/methodParameters/createTicketIn";
import { createTicketsOut } from "src/app/shared/domain/methodParameters/createTicketOut";
import { deleteTicketIn } from "src/app/shared/domain/methodParameters/deleteTicketIn";
import { deleteTicketOut } from "src/app/shared/domain/methodParameters/deleteTicketOut";
import { getTicektsIn } from "src/app/shared/domain/methodParameters/getTicektsIn";
import { getTicketsOut } from "src/app/shared/domain/methodParameters/getTicketsOut";
import { updateTicketIn } from "src/app/shared/domain/methodParameters/updateTicketIn";
import { updateTicketOut } from "src/app/shared/domain/methodParameters/updateTicketOut";
import { ShippingRoutesGateway } from "../../domain/gateway/shipping-routes-gateway";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BaseApiService } from "src/app/shared/base-api.service";
@Injectable()
export class ShippingRoutesApiService extends ShippingRoutesGateway{

    constructor(private http: BaseApiService,
        private https: HttpClient) { super();}
        
     CreateTicket(path?: string | undefined, input?: createTicketsIn | undefined, token?: string | undefined): Observable<createTicketsOut> {
        const headers = new HttpHeaders({ 'Authorization': 'Bearer ' + token });
        return this.http.post(path, input,headers).pipe(map(((Response) => Response as createTicketsOut)));
    
    }
     UpdateTicket(path?: string | undefined, input?: updateTicketIn | undefined, token?: string | undefined): Observable<updateTicketOut> {
        const headers = new HttpHeaders({ 'Authorization': 'Bearer ' + token });
        return this.http.post(path, input,headers).pipe(map(((Response) => Response as updateTicketOut)));
    }
     GetAllTicket(path?: string | undefined, input?: getTicektsIn | undefined, token?: string | undefined): Observable<getTicketsOut> {
        const headers = new HttpHeaders({ 'Authorization': 'Bearer ' + token });
        return this.http.post(path, input,headers).pipe(map(((Response) => Response as getTicketsOut)));
    }
     GetTicketByID(path?: string | undefined, input?: getTicektsIn | undefined, token?: string | undefined): Observable<getTicketsOut> {
        const headers = new HttpHeaders({ 'Authorization': 'Bearer ' + token });
        return this.http.post(path, input,headers).pipe(map(((Response) => Response as getTicketsOut)));
    }
     DeleteTicket(path?: string | undefined, input?: deleteTicketIn | undefined, token?: string | undefined): Observable<deleteTicketOut> {
        const headers = new HttpHeaders({ 'Authorization': 'Bearer ' + token });
        return this.http.post(path, input,headers).pipe(map(((Response) => Response as deleteTicketOut)));
    }

}
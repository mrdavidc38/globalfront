import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShippingticketsRoutingModule } from './infraestruture/shippingtickets-routing.module';
import { ShippingUseCase } from './domain/usecase/shipping-use-case';
import { ShippingRoutesGateway } from './domain/gateway/shipping-routes-gateway';
import { ShippingRoutesApiService } from './infraestruture/driven-adapter/shipping-routes-api.service';



@NgModule({
  
  imports: [
    CommonModule,
    ShippingticketsRoutingModule
  ],
  exports : [
    
  ]
  ,
  providers:[
    ShippingUseCase,
    {provide: ShippingRoutesGateway, useClass: ShippingRoutesApiService}
  ]
}) 
export class ShippingticketsModule { }

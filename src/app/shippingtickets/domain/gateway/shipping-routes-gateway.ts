import { Observable } from "rxjs";
import { createTicketsIn } from "src/app/shared/domain/methodParameters/createTicketIn";
import { createTicketsOut } from "src/app/shared/domain/methodParameters/createTicketOut";
import { deleteTicketIn } from "src/app/shared/domain/methodParameters/deleteTicketIn";
import { deleteTicketOut } from "src/app/shared/domain/methodParameters/deleteTicketOut";
import { getTicektsIn } from "src/app/shared/domain/methodParameters/getTicektsIn";
import { getTicketsOut } from "src/app/shared/domain/methodParameters/getTicketsOut";
import { updateTicketIn } from "src/app/shared/domain/methodParameters/updateTicketIn";
import { updateTicketOut } from "src/app/shared/domain/methodParameters/updateTicketOut";

export abstract class ShippingRoutesGateway
{
    abstract CreateTicket(path? : string, input? : createTicketsIn , token? : string): Observable<createTicketsOut>;
    abstract UpdateTicket(path? : string, input? : updateTicketIn,token? : string): Observable<updateTicketOut>;
    abstract GetAllTicket(path? : string, input? : getTicektsIn,token? : string): Observable<getTicketsOut>;
    abstract GetTicketByID(path? : string, input? : getTicektsIn,token? : string): Observable<getTicketsOut>;
    abstract DeleteTicket(path? : string, input? : deleteTicketIn,token? : string): Observable<deleteTicketOut>;

}
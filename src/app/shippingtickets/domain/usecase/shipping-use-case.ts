import { Observable, map } from "rxjs";
import { CreateUserIn } from "src/app/shared/domain/methodParameters/CreateUserIn";
import { CreateUserOut } from "src/app/shared/domain/methodParameters/CreateUserOut";
import { GetUserIn } from "src/app/shared/domain/methodParameters/GetUserIn";
import { GetUserOut } from "src/app/shared/domain/methodParameters/GetUserOut";
import { UpdateUserIn } from "src/app/shared/domain/methodParameters/UpdateUserIn";
import { UpdateUserOut } from "src/app/shared/domain/methodParameters/UpdateUserOut";
import { HttpClient } from "@angular/common/http";
import { BaseApiService } from "src/app/shared/base-api.service";
import { Injectable } from "@angular/core";
import { GetUserByIDOut } from "src/app/shared/domain/methodParameters/GetUserByIDOut";
import { createTicketsIn } from "src/app/shared/domain/methodParameters/createTicketIn";
import { createTicketsOut } from "src/app/shared/domain/methodParameters/createTicketOut";
import { updateTicketOut } from "src/app/shared/domain/methodParameters/updateTicketOut";
import { updateTicketIn } from "src/app/shared/domain/methodParameters/updateTicketIn";
import { deleteTicketIn } from "src/app/shared/domain/methodParameters/deleteTicketIn";
import { deleteTicketOut } from "src/app/shared/domain/methodParameters/deleteTicketOut";
import { getTicektsIn } from "src/app/shared/domain/methodParameters/getTicektsIn";
import { getTicketsOut } from "src/app/shared/domain/methodParameters/getTicketsOut";
import { ShippingRoutesGateway } from "../gateway/shipping-routes-gateway";
@Injectable()
export class ShippingUseCase 
{
    constructor(private _shippingGateway : ShippingRoutesGateway)
    {  
    }
     CreateTicket(path?: string | undefined, input?: createTicketsIn | undefined, token? : string): Observable<createTicketsOut> {
        return this._shippingGateway.CreateTicket('Tickets/CreateTicket', input,token);
    }
     UpdateTicket(path?: string | undefined, input?: updateTicketIn | undefined, token? : string): Observable<updateTicketOut> {
        return this._shippingGateway.UpdateTicket('Tickets/UpdateTicket', input,token);
    }
     GetTicketAll(path?: string | undefined, input?: getTicektsIn | undefined,token? : string): Observable<getTicketsOut> {
        return this._shippingGateway.GetAllTicket('Tickets/GetAllTicket', input, token);
    }
     GetTicketByID(path?: string | undefined, input?: getTicektsIn | undefined, token? : string): Observable<getTicketsOut> {
        return this._shippingGateway.GetTicketByID('Tickets/GetTicketByID', input,token);
    }
    DeleteTicket(path?: string | undefined, input?: deleteTicketIn | undefined, token? : string) : Observable<deleteTicketOut>
    {
        return this._shippingGateway.DeleteTicket('Tickets/DeleteTicket', input,token);

    }
        

}
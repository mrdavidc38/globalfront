import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { SharedModule } from '../shared/shared.module';
import { LayoutComponent } from './ui/layout.component';
import { UserModule } from '../user/user.module';
import { UserComponent } from '../user/ui/user.component';
import { UserRoutesGateway } from '../user/domain/gateway/user-routes-gateway';
import { UserUseCase } from '../user/domain/use-cases/user-use-case';
import { UserRoutesApiService } from '../user/driven-adapter/user-routes-api.services';
import { ShippingticketsModule } from '../shippingtickets/shippingtickets.module';
import { ShippingticketsComponent } from '../shippingtickets/ui/shippingtickets.component';


@NgModule({
  declarations: [
    LayoutComponent,
    UserComponent,
    ShippingticketsComponent,
  ],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    SharedModule,
    UserModule,
    ShippingticketsModule
  ]
  ,exports : [
    LayoutComponent,
    UserComponent,
    ShippingticketsComponent
  ],
  providers : [
    UserUseCase,
    {provide: UserRoutesGateway, useClass: UserRoutesApiService}

  ]
})
export class LayoutModule { }

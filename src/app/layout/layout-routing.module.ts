import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './ui/layout.component';
import { UserComponent } from '../user/ui/user.component';
import { ShippingticketsComponent } from '../shippingtickets/ui/shippingtickets.component';
import { permissions } from '../shared/permissions/permissions.guard';

const routes: Routes = [
  {
    path : '',
    component : LayoutComponent,
    canActivate:[permissions],
    children:[
      {path :'user', component:UserComponent},
      {path :'ticket', component:ShippingticketsComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AdministrationModule } from './administration/administration.module';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AdministrationGateway } from './administration/domain/gateway/administration-routes-gateway';
import { AdministrationUsecase } from './administration/domain/usecase/administration-use-case';
import { AdministrationRoutesApiService } from './administration/infraestructure/driven-adapter/administration-routes-api.service';
import { SharedModule } from './shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutComponent } from './layout/ui/layout.component';
import { UserUseCase } from './user/domain/use-cases/user-use-case';
import { UserRoutesGateway } from './user/domain/gateway/user-routes-gateway';
import { UserRoutesApiService } from './user/driven-adapter/user-routes-api.services';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { userReducer } from './store/reducers/user.reducer';
import { environment } from './environments/environment';
import { ShippingRoutesGateway } from './shippingtickets/domain/gateway/shipping-routes-gateway';
import { ShippingUseCase } from './shippingtickets/domain/usecase/shipping-use-case';
import { ShippingRoutesApiService } from './shippingtickets/infraestruture/driven-adapter/shipping-routes-api.service';

@NgModule({
  declarations: [
    AppComponent,
   
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    SharedModule,
    BrowserAnimationsModule,
    StoreModule.forRoot({
      'user': userReducer,

    }),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: false,
      autoPause: true,
    }),
  ],
  
  
  providers: [
    AdministrationUsecase,
    {provide: AdministrationGateway, useClass: AdministrationRoutesApiService},
    UserUseCase,
    {provide: UserRoutesGateway, useClass: UserRoutesApiService},
    ShippingUseCase,
    {provide: ShippingRoutesGateway, useClass: ShippingRoutesApiService}

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

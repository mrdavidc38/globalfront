import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './administration/ui/login/login.component';
import { LayoutComponent } from './layout/ui/layout.component';

export const routes: Routes = [
  {
    path: '',
    component : LoginComponent,
    loadChildren: () => import('./administration/administration.module').then(module => module.AdministrationModule),
   },
  // ,
  {
    path: '',
    loadChildren: () => import('./user/user.module').then(module => module.UserModule),
  },
  {
    path: '',
    loadChildren: () => import('./shippingtickets/shippingtickets.module').then(module => module.ShippingticketsModule),
  },
  
  {
    path: 'pages',
    loadChildren: () => import('./layout/layout.module').then(module => module.LayoutModule)
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes,{ useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

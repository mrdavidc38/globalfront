import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from '../ui/user.component';

const routes: Routes = [
  // {
  //   path: 'User',
  //   component : UserComponent,
  //   loadChildren : () => import('src/app/user/user.module').then(module => module.UserModule)
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }

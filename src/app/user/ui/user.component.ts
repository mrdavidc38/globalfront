import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { User } from 'src/app/shared/domain/entities/User';
import { ModalServiceService } from 'src/app/shared/modalService/modal-service.service';
import { UserRoutesApiService } from '../driven-adapter/user-routes-api.services';
import { GetUserIn } from 'src/app/shared/domain/methodParameters/GetUserIn';
import { Profile } from 'src/app/shared/domain/entities/Profile';
import { UserUseCase } from '../domain/use-cases/user-use-case';
import { LoginOut } from 'src/app/shared/domain/methodParameters/LoginOut';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { getAll } from 'src/app/store/selector/user.selector';
import { getLocaleDateFormat } from '@angular/common';
import { UpdateUserIn } from 'src/app/shared/domain/methodParameters/UpdateUserIn';
import { Result } from 'src/app/shared/domain/entities/Result';
import { CreateUserIn } from 'src/app/shared/domain/methodParameters/CreateUserIn';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
 
  columnasTabla : string[] = ['nombreCompleto','correo','estado','acciones'];
  dataInicio :any= [{usuID :0,usuName:'usuName', usuEmail: 'correo', usuStatus : 'usuStatus',usuPassword:'usuPassword', usuUserMod:0, usuUserModDate:0,usuCreationDate:0, proID :0}];
  dataListaUsuarios = new MatTableDataSource(this.dataInicio);
  formularioUsuarioEdit! : FormGroup;
  formularioUsuarioCreate ! : FormGroup;
  ocultarPassword : boolean = true;
  tituliAction : string = "Agregar";
  botonAction : string ="Guardar";
  token : any ;
  UserList : User[] = [];
  // @ViewChild('editModalProduct') editModalProduct!: ModalComponent;
  constructor(private fb : FormBuilder,private _modal : ModalServiceService,
    private userServide : UserUseCase,
     private router : Router, private store: Store<LoginOut>)
  {
    this.formularioUsuarioEdit = fb.group({
      usuID :[,Validators.required],
      nombreCompleto :[,Validators.required],
      correo :[,Validators.required],
     
      
      esActivo :["1"],
    });

    this.formularioUsuarioCreate = fb.group({
      nombreCompleto :[,Validators.required],
      correo :[,Validators.required],
      esActivo :["1"],
      password :[,Validators.required],
    });
  }
  ngOnInit(): void {
setTimeout(() => {
  this.store.select(getAll).pipe().subscribe(local =>{
    
    this.token = local
  }); 
  
this.getUser();
}, 500);
  }

  nuevoUsuario()
  {
    this._modal.openDialogCustom();
  
  }
  editarUsuario(input :User, template: TemplateRef<any>)
  {

    this.openModal(template);
    this.formularioUsuarioEdit.get('usuID')?.patchValue(input.usuID);

    this.formularioUsuarioEdit.get('nombreCompleto')?.patchValue(input.usuName);
    this.formularioUsuarioEdit.get('correo')?.patchValue(input.usuEmail);
    this.formularioUsuarioEdit.get('esActivo')?.patchValue(input.usuName);
    
  }
 Editar_Usuario()
  {
    var d = new Date()
    var userUPdate : User ={
      usuID: this.formularioUsuarioEdit.get('usuID')?.value,
      usuName:this.formularioUsuarioEdit.get('nombreCompleto')?.value,
      usuEmail:    this.formularioUsuarioEdit.get('correo')?.value,
      
      usuStatus:     this.formularioUsuarioEdit.get('esActivo')?.value == 1 ?'V' :'I',
      
      usuUserCreation: 0,
      usuPassword: 'q',
      usuUserMod: 0,
      usuUserModDate: 0,
      usuCreationDate: d,
      proID: 0
    }
    var UpdateUser : UpdateUserIn ={
      user: userUPdate
    }
    this.userServide.UpdateUser('',UpdateUser,this.token.user.token).subscribe({
      next : (data) =>{
          if(data.result = Result.Success)
          {
           
          }
      }
      ,complete: ()  =>{
        this.getUser();
        this.modalCLose();
      }
    });

  }
  eliminarUsuario(input : Event)
  {}
  aplicarFiltroTabla(event : Event)
  {
      const filterValue =(event.target as HTMLInputElement).value;
      this.dataListaUsuarios.filter = filterValue.trim().toLocaleLowerCase();
  }

  openModal(TemplateRef : TemplateRef<any>)
  {
   
    this._modal.opendialogtemplate({template:TemplateRef}).afterClosed()
    .subscribe(t =>{});
  }
  modalCLose()
  {
    this._modal.closeDialogtemplate();
  }
  getUser()
  {
    var d = new Date()
    var profile : Profile = {
      proID: 0,
      proName: ''
    }
    var user : User = {
      usuID: 0,
      usuName: '',
      usuEmail: '',
      usuStatus: '',
      usuUserCreation: 0,
      usuPassword: '',
      usuUserMod: 0,
      usuUserModDate: 0,
      usuCreationDate: d,
      proID: 0
    }
    var getUserIn : GetUserIn = {
      user: user
    }
    
    this.userServide.GetUserByID('',getUserIn, this.token.user.token).subscribe( {
      next: (data)=> {
        
      this.dataInicio = data.user;
      },
      complete: () =>{
      
      
      }

    })
  }

  guardar_Usuario()
  {
    var d = new Date()

    var userCreate : User ={
      usuID: 0,
      usuName:this.formularioUsuarioCreate.get('nombreCompleto')?.value,
      usuEmail:this.formularioUsuarioCreate.get('correo')?.value,
      
     usuStatus: this.formularioUsuarioCreate.get('esActivo')?.value == 1 ?'V' :'I',
      
      usuUserCreation: 0,
      usuPassword: this.formularioUsuarioCreate.get('password')?.value ,
      usuUserMod: 0,
      usuUserModDate: 0,
      usuCreationDate: d,
      proID: 0
    }

   var createUser : CreateUserIn = {
     user: userCreate
   }
    this.userServide.CreateUser('',createUser, this.token.user.token).subscribe({
      next: (data) =>{
        if(data.result = Result.Success)
        {}
      }
      ,complete: () =>{
        this.getUser();
        this.modalCLose();
      }
    })
  }
}

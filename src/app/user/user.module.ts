import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { SharedModule } from '../shared/shared.module';
import { UserComponent } from './ui/user.component';
import { UserRoutingModule } from './infraestructure/user-routing.module';
import { UserUseCase } from './domain/use-cases/user-use-case';
import { AdministrationGateway } from '../administration/domain/gateway/administration-routes-gateway';
import { AdministrationRoutesApiService } from '../administration/infraestructure/driven-adapter/administration-routes-api.service';
import { UserRoutesGateway } from './domain/gateway/user-routes-gateway';
import { UserRoutesApiService } from './driven-adapter/user-routes-api.services';



@NgModule({
  declarations: [
 
  
   
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule
  ]
  ,
  exports : [
    
  ],
  providers:[
    UserUseCase,
    {provide: UserRoutesGateway, useClass: UserRoutesApiService}
  ]
})
export class UserModule { }
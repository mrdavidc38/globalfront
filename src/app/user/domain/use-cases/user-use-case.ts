import { Observable, map } from "rxjs";
import { CreateUserIn } from "src/app/shared/domain/methodParameters/CreateUserIn";
import { CreateUserOut } from "src/app/shared/domain/methodParameters/CreateUserOut";
import { GetUserIn } from "src/app/shared/domain/methodParameters/GetUserIn";
import { GetUserOut } from "src/app/shared/domain/methodParameters/GetUserOut";
import { UpdateUserIn } from "src/app/shared/domain/methodParameters/UpdateUserIn";
import { UpdateUserOut } from "src/app/shared/domain/methodParameters/UpdateUserOut";
import { UserRoutesGateway } from "../gateway/user-routes-gateway";
import { HttpClient } from "@angular/common/http";
import { BaseApiService } from "src/app/shared/base-api.service";
import { Injectable } from "@angular/core";
import { GetUserByIDOut } from "src/app/shared/domain/methodParameters/GetUserByIDOut";
@Injectable()
export class UserUseCase 
{
    constructor(private _userGateway : UserRoutesGateway)
    {  
    }
     CreateUser(path?: string | undefined, input?: CreateUserIn | undefined, token? : string): Observable<CreateUserOut> {
        return this._userGateway.CreateUser('user/CreateUser', input,token);
    }
     UpdateUser(path?: string | undefined, input?: UpdateUserIn | undefined, token? : string): Observable<UpdateUserOut> {
        return this._userGateway.UpdateUser('user/UpdateUser', input,token);
    }
     GetUser(path?: string | undefined, input?: GetUserIn | undefined,token? : string): Observable<GetUserOut> {
        return this._userGateway.GetUser('user/GetUser', input, token);
    }
     GetUserByID(path?: string | undefined, input?: GetUserIn | undefined, token? : string): Observable<GetUserByIDOut> {
        return this._userGateway.GetUserByID('user/GetUserByID', input,token);
    }
        

}
import { Observable } from "rxjs";
import { CreateUserIn } from "src/app/shared/domain/methodParameters/CreateUserIn";
import { CreateUserOut } from "src/app/shared/domain/methodParameters/CreateUserOut";
import { GetUserByIDOut } from "src/app/shared/domain/methodParameters/GetUserByIDOut";
import { GetUserIn } from "src/app/shared/domain/methodParameters/GetUserIn";
import { GetUserOut } from "src/app/shared/domain/methodParameters/GetUserOut";
import { UpdateUserIn } from "src/app/shared/domain/methodParameters/UpdateUserIn";
import { UpdateUserOut } from "src/app/shared/domain/methodParameters/UpdateUserOut";

export abstract class UserRoutesGateway
{
    abstract CreateUser(path? : string, input? : CreateUserIn , token? : string): Observable<CreateUserOut>;
    abstract UpdateUser(path? : string, input? : UpdateUserIn,token? : string): Observable<UpdateUserOut>;
    abstract GetUser(path? : string, input? : GetUserIn,token? : string): Observable<GetUserOut>;
    abstract GetUserByID(path? : string, input? : GetUserIn,token? : string): Observable<GetUserByIDOut>;

}
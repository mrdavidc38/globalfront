import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, map } from "rxjs";
import { BaseApiService } from "src/app/shared/base-api.service";
import { CreateUserIn } from "src/app/shared/domain/methodParameters/CreateUserIn";
import { CreateUserOut } from "src/app/shared/domain/methodParameters/CreateUserOut";
import { GetUserIn } from "src/app/shared/domain/methodParameters/GetUserIn";
import { GetUserOut } from "src/app/shared/domain/methodParameters/GetUserOut";
import { UpdateUserIn } from "src/app/shared/domain/methodParameters/UpdateUserIn";
import { UpdateUserOut } from "src/app/shared/domain/methodParameters/UpdateUserOut";
import { UserRoutesGateway } from "../domain/gateway/user-routes-gateway";
import { GetUserByIDOut } from "src/app/shared/domain/methodParameters/GetUserByIDOut";

@Injectable()
export class UserRoutesApiService extends UserRoutesGateway{

    constructor(private http: BaseApiService,
        private https: HttpClient) { super();}

   CreateUser(path?: string | undefined, input?: CreateUserIn | undefined,token?: String): Observable<CreateUserOut> {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer ' + token });

    return this.http.post(path, input,headers).pipe(map(((Response) => Response as CreateUserOut)));
  }
   UpdateUser(path?: string | undefined, input?: UpdateUserIn | undefined,token?: String): Observable<UpdateUserOut> {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer ' + token });
    return this.http.post(path, input,headers).pipe(map(((Response) => Response as UpdateUserOut)));
  }
   GetUser(path?: string | undefined, input?: GetUserIn | undefined,token?: String): Observable<GetUserOut> {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer ' + token });
    return this.http.post(path, input, headers).pipe(map(((Response) => Response as GetUserOut)));
  }
   
  GetUserByID(path?: string | undefined, input?: GetUserIn | undefined,token?: String): Observable<GetUserByIDOut> {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer ' + token });
    return this.http.post(path, input, headers).pipe(map(((Response) => Response as GetUserByIDOut)));
  }

  }
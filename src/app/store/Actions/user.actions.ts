import { createAction, props } from "@ngrx/store";
import { User } from "src/app/shared/domain/entities/User";
import { LoginOut } from "src/app/shared/domain/methodParameters/LoginOut";

export const get = createAction('[LoginOut]Get');
export const update = createAction('[LoginOut] update' ,props<LoginOut>())
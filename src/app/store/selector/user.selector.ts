import { createFeatureSelector, createSelector } from "@ngrx/store";
import { User } from "src/app/shared/domain/entities/User";
import { LoginOut } from "src/app/shared/domain/methodParameters/LoginOut";

export const getUser = (state : LoginOut) =>  state;

export const getAll = createSelector(
    getUser,
    (state : LoginOut) => {
        return state
    }
)
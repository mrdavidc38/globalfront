import { createReducer, on } from "@ngrx/store";
import { Profile } from "src/app/shared/domain/entities/Profile";
import { User } from "src/app/shared/domain/entities/User";
import * as UserActionTypes from '../Actions/user.actions';
import { LoginOut } from "src/app/shared/domain/methodParameters/LoginOut";


export const initialState : LoginOut ={
    token: "",
    name: "",
    firstSurname: "",
    secondSurname: "",
    usuID: 0,
    proID: 0,
    usuPhone: "",
    proName: ""
}

export const userReducer = createReducer(
    initialState,
    on(UserActionTypes.get, state =>({...state})),
    on(UserActionTypes.update, (state, LoginOut) =>(LoginOut))
);